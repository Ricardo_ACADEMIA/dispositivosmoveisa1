# README #

Código para a aula 1 de desenvolvimento de apps Android. Este código foi gerado automaticamente pelo Android Studio, utilizando o template "Empty Activity".

### What is this repository for? ###

Este projeto é o arquivo base da aula 1 da matéria de Dispositivos Móveis e serve duas funcionalidades:

1. Servir como prática para a utilização de repositórios git;
1. Prover uma base inicial para o projeto a ser desenvolvido em aula.

### How do I get set up? ###

Para clonar o repositório git no Android Studio:

1. Clique no menu _F_ile > New > Project From Version Control... > Git.
1. Na janela que abrir, entre com o endereço https://Ricardo_ACADEMIA@bitbucket.org/Ricardo_ACADEMIA/dispositivosmoveisa1.git

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Repositório administrado por
Ricardo César Ribeiro dos Santos
ricardo.santos@bagozzi.edu.br